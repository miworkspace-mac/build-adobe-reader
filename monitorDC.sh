#!/bin/bash -ex

URL=`./finderDC.rb`

if [ "x${URL}" != "x" ]; then
    echo URL: "${URL}"
    echo "${URL}" > current-urldc
fi

# Update to handle distributed builds
if cmp current-urldc old-urldc; then
    # Files are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-urldc old-urldc
    exit 0
fi