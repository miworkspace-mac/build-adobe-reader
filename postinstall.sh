#!/bin/bash -ex

DISABLED="/Applications/Adobe Acrobat Reader DC.app/Contents/Plugins/Updater.disabled"

if [ -d "${DISABLED}" ]; then
	echo "Previous Version Detected, Removing"
	rm -rf "${DISABLED}"
else
	echo "Previous Version Does Not Exist"
fi

mv "/Applications/Adobe Acrobat Reader DC.app/Contents/Plugins/Updater.acroplugin" "${DISABLED}"

exit 0