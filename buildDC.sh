#!/bin/bash -ex

# CONFIG
prefix="AdobeReaderDC"
suffix=""
munki_package_name="AdobeReaderDCUpdate"
category="Productivity"
url=`./finderDC.sh`
# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15' "${url}"

## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
cp "${mountpoint}"/* ReaderDC.pkg
hdiutil detach "${mountpoint}"

/usr/sbin/pkgutil --expand ReaderDC.pkg pkg
pax -rz -f pkg/application.pkg/Payload

mkdir -p build-root/Applications
cp -R Adobe*.app build-root/Applications

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" build-root/Applications/Adobe*.app/Contents/Info.plist`

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root ReaderDC.pkg ${key_files} --preinstall_script preinstall.sh --postinstall_script postinstall.sh | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.10.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "Adobe Reader DC"
defaults write "${plist}" update_for -array AdobeReaderDC AdobeReader

# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool AdobeReaderDC`
defaults write "${plist}" display_name -string "${displayname}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool AdobeReaderDC`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool AdobeReaderDC`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv ReaderDC.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
