#!/bin/bash

NEWLOC=`curl -L "https://get.adobe.com/reader/" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Safari/605.1.15' | grep _for_Mac_Intel | awk -F'"' '{print $2}'`

NEWLOC2="https://get.adobe.com/reader/download/?installer="${NEWLOC}"&stype=7718&standalone=1"
if [ "x${NEWLOC2}" != "x" ]; then
	echo "${NEWLOC2}"
fi
