#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

begin
  doc = Nokogiri::HTML(open("https://supportdownloads.adobe.com/product.jsp?product=10&platform=Macintosh", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
  puts "Failed to fetch download page"
  exit 1
end

link = doc.xpath('//a').find {|x| x.inner_html.match(/Adobe Acrobat Reader DC.*update/i) }

# Goes to page to and finds the next linky
begin
  doctwo = Nokogiri::HTML(open("https://supportdownloads.adobe.com/#{link['href']}", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
  puts "Failed to fetch download page #2"
  exit 1
end

linktwo = doctwo.xpath('//a').find {|x| x.inner_html.match(/Proceed to Download/i) }

# Goes to a final page and finds the donwload linky
begin
  docfinal = Nokogiri::HTML(open("https://supportdownloads.adobe.com/#{linktwo['href']}", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
  puts "Failed to fetch download page #3"
  exit 1
end

linkfinal = docfinal.xpath('//a').find {|x| x.inner_html.match(/Download Now/i) }

if linkfinal
    puts "#{linkfinal['href']}"
end


